const _partialScenarios = require("./scenarios");
const _paths = require("./paths.json");
let _scenarios = [];

const _defaultScenario = {
  "label": "", // Required! Will be set later
  "cookiePath": "tests/backstopjs/cookies/default.json",
  "referenceUrl": "https://www.fremo-net.eu",
  "url": "https://76.fremo-net.eu",
  "selectors": ["document"],
  "misMatchThreshold" : 0.025,
  "delay": 1000,
  "requireSameDimensions": true
};

// Step 1: we collect all the scenario (fragments), merge this with defaults
// and define urls and label
_partialScenarios.forEach(function (_scenario, _i) {
  // Merge the default scenario (clone, not reference)
  _scenario = Object.assign({}, _defaultScenario, _scenario);
  _scenario.label = _i + _scenario.path;
  _scenario.url += _scenario.path;
  _scenario.referenceUrl += _scenario.path;
  _scenarios.push(_scenario);
});

// Step 2: create scenarios from all the given paths
_paths.forEach(function(_path, _i) {
  // Skip if we have the scenario already in the list
  if (_scenarios.find(_x => _x.path === _path) !== undefined) {
    return;
  }
  // Copy the default scenario (clone, not reference)
  let _scenario = Object.assign({}, _defaultScenario);
  _scenario.label = _i + _path;
  _scenario.url += _path;
  _scenario.referenceUrl += _path;
  _scenarios.push(_scenario);
});

//console.log(_scenarios);
//throw new Error();

module.exports = {
  "id": "test",
  "viewports": [
    {
      "label": "tablet-landscape",
      "width": 1024,
      "height": 768
    }
  ],
  "onBeforeScript": "puppet/onBefore.js",
  "scenarios": _scenarios,
  "paths": {
    "bitmaps_reference": "tests/backstopjs/bitmaps_reference",
    "bitmaps_test": "backstop_data/bitmaps_test",
    "engine_scripts": "tests/backstopjs/engine_scripts",
    "html_report": "backstop_data/html_report",
    "ci_report": "backstop_data/ci_report"
  },
  "report": ["browser"],
  "engine": "puppeteer",
  "engineOptions": {
    "args": ["--no-sandbox"]
  },
  "asyncCaptureLimit": 5,
  "asyncCompareLimit": 50,
  "debug": false,
  "debugWindow": false
};
