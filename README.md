﻿# Monitoring für fremo-net.eu

Akzeptanz-Tests der FREMO-Webseite mit BackstopJS.

Vor allem gebraucht für TYPO3-Upgrades

**Wichtig:  
BackstopJS-Versionen in [packages.json](packages.json) und [.gitlab-ci.yml](.gitlab-ci.yml) synchron halten!**

## Anforderungen

**Visual Regression Tests &ndash; BackstopJS**

* npm >= 6.9.0 (andere Versionen wurden nicht getestet)
* Docker  
  Um mit BackstopJS systemunabhängig einheitliche Ergebnisse zu bekommen

## Lokales Ausführen

### BackstopJS

**Die Konfiguration liegt in `tests/backstopjs/config.js`!**

#### In Docker

Um einen Vergleich durch GitLab CI zu ermöglichen, sollten auch auch lokal nur 
mit Docker gearbeitet werden. Das führt unter der GitBash zu einem Fehler, 
daher muss BackstopJS  in der PowerShell ausgeührt werden:

Um gegen die aktuellen Referenzen zu testen:  
(Es empfiehlt sich `"report": ["browser"]` zu setzen)

```PS C:\Path\to\fremo-net-monitoring> node_modules/.bin/backstop test --config=tests/backstopjs/config --docker```

Um neue Referenzen anzulegen:

```PS C:\Path\to\fremo-net-monitoring> node_modules/.bin/backstop reference --config=tests/backstopjs/config --docker```
